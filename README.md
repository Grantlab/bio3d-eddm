
Bio3D Ensemble Difference Distance Matrix Analysis
==================================================

Bio3D-eDDM is an [R](http://www.r-project.org/) package that compares residue-residue atomic distances across multiple structure sets to identify significant conformational changes that may underlie certain functional processes.

It is a package of the larger [Bio3D](https://bitbucket.org/Grantlab/bio3d) family containing utilities for the analysis of protein structure, sequence and trajectory data. It is currently distributed as platform independent source code under the [GPL version 2 license](http://www.gnu.org/copyleft/gpl.html).

Installation
------------

The development version of Bio3d-nma is available from BitBucket:

``` r
install.packages("devtools")
devtools::install_bitbucket("Grantlab/bio3d-eddm")
```

Make sure you have installed [Bio3D](https://bitbucket.org/Grantlab/bio3d) core package prior to installing Bio3D-eddm:

``` r
devtools::install_bitbucket("Grantlab/bio3d/bio3d-core", ref="core")
```

Basic usage
-----------

``` r
library(bio3d)
library(bio3d.eddm)

ids <- c("1rx2_A", "1rx8_A", "2ano_A", "1dre_A", "1rd7_B", "4fhb_A", "1dra_A", "1tdr_B")
files <- get.pdb(ids, split = TRUE, path = tempdir())
#> 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |=========                                                             |  12%
  |                                                                            
  |==================                                                    |  25%
  |                                                                            
  |==========================                                            |  38%
  |                                                                            
  |===================================                                   |  50%
  |                                                                            
  |============================================                          |  62%
  |                                                                            
  |====================================================                  |  75%
  |                                                                            
  |=============================================================         |  88%
  |                                                                            
  |======================================================================| 100%

## Sequence Alignement
pdbs <- pdbaln(files, outfile = tempfile())
#> Reading PDB files:
#> /tmp/RtmpCe6KQy/split_chain/1rx2_A.pdb
#> /tmp/RtmpCe6KQy/split_chain/1rx8_A.pdb
#> /tmp/RtmpCe6KQy/split_chain/2ano_A.pdb
#> /tmp/RtmpCe6KQy/split_chain/1dre_A.pdb
#> /tmp/RtmpCe6KQy/split_chain/1rd7_B.pdb
#> /tmp/RtmpCe6KQy/split_chain/4fhb_A.pdb
#> /tmp/RtmpCe6KQy/split_chain/1dra_A.pdb
#> /tmp/RtmpCe6KQy/split_chain/1tdr_B.pdb
#>    PDB has ALT records, taking A only, rm.alt=TRUE
#> ..   PDB has ALT records, taking A only, rm.alt=TRUE
#> ......
#> 
#> Extracting sequences
#> 
#> pdb/seq: 1   name: /tmp/RtmpCe6KQy/split_chain/1rx2_A.pdb 
#>    PDB has ALT records, taking A only, rm.alt=TRUE
#> pdb/seq: 2   name: /tmp/RtmpCe6KQy/split_chain/1rx8_A.pdb 
#> pdb/seq: 3   name: /tmp/RtmpCe6KQy/split_chain/2ano_A.pdb 
#>    PDB has ALT records, taking A only, rm.alt=TRUE
#> pdb/seq: 4   name: /tmp/RtmpCe6KQy/split_chain/1dre_A.pdb 
#> pdb/seq: 5   name: /tmp/RtmpCe6KQy/split_chain/1rd7_B.pdb 
#> pdb/seq: 6   name: /tmp/RtmpCe6KQy/split_chain/4fhb_A.pdb 
#> pdb/seq: 7   name: /tmp/RtmpCe6KQy/split_chain/1dra_A.pdb 
#> pdb/seq: 8   name: /tmp/RtmpCe6KQy/split_chain/1tdr_B.pdb

pdbs.aa <- read.all(pdbs)

# calculate the distance matrices
dm <- dm(pdbs.aa$all, grpby=pdbs.aa$all.grpby, ncore=NULL)

# group structures based on ligand binding states
grps <- c(1,1,1,1,2,2,2,2)

# eDDM with masked DMs
res <- eddm(pdbs.aa, dm=dm, grps=grps)
#>  .. wilcox test - testing for significance between groups
#>  .. calculating mean and S.D. of distances
#>  .. calculating difference in means
#>  .. calculating z-score between groups
#>  .. calculating mean effective distance
#>  .. calculating difference in mean effective distance
head(res)
#> 
#> Class:
#>   eddm, data.frame
#> 
#> # Residue pairs:
#>   6 (7 unique residues)
#> 
#> # Groups:
#>   2 (1, 2)
#> 
#>            a        b i  j  d.1  d.2 sd.1 sd.2 dm.1_2 z.1_2 md.1 md.2 mdm.1_2
#> 1-4  MET1(A)  LEU4(A) 1  4 7.70 7.58 0.12 0.10   0.12  0.96 6.53 6.51    0.02
#> 1-38 MET1(A) LYS38(A) 1 38 7.36 7.46 0.76 0.39   0.09  0.12 6.39 6.47    0.08
#> 1-39 MET1(A) PRO39(A) 1 39 7.62 7.58 0.32 0.11   0.04  0.14 6.50 6.51    0.01
#> 1-41 MET1(A) ILE41(A) 1 41 8.25 8.27 0.32 0.26   0.02  0.05 6.55 6.55    0.00
#> 1-82 MET1(A) ILE82(A) 1 82 7.79 8.15 0.24 0.24   0.36  1.50 6.53 6.55    0.02
#> 1-85 MET1(A) CYS85(A) 1 85 8.01 7.77 0.26 0.32   0.24  0.92 6.54 6.52    0.02
#>      pv.1_2
#> 1-4    0.20
#> 1-38   0.89
#> 1-39   1.00
#> 1-41   1.00
#> 1-82   0.10
#> 1-85   0.46

# focus on the comparison between GTP and GDI
res2 <- subset.eddm(res, grps=c(1,2), alpha=0.05, beta=1.0)
head(res2)
#> 
#> Class:
#>   eddm, data.frame
#> 
#> # Residue pairs:
#>   6 (11 unique residues)
#> 
#> # Groups:
#>   2 (1, 2)
#> 
#>               a         b  i   j  d.1  d.2 sd.1 sd.2 dm.1_2 z.1_2 md.1 md.2
#> 18-45  ASN18(A)  HIS45(A) 18  45 3.05 9.56 0.17 2.11   6.52 37.26 3.05 6.48
#> 18-49  ASN18(A)  SER49(A) 18  49 2.96 7.32 0.35 2.00   4.36 12.53 2.96 6.10
#> 15-19  GLY15(A)  ALA19(A) 15  19 3.43 6.98 0.32 0.50   3.55 11.23 3.43 6.31
#> 22-148 TRP22(A) SER148(A) 22 148 6.71 3.47 0.35 0.11   3.24  9.35 6.21 3.47
#> 21-149 PRO21(A) HIS149(A) 21 149 6.76 3.62 0.98 0.31   3.15  3.22 6.14 3.62
#> 20-119 MET20(A) VAL119(A) 20 119 6.83 3.98 0.30 0.07   2.85  9.58 6.27 3.98
#>        mdm.1_2 pv.1_2
#> 18-45     3.43  0.027
#> 18-49     3.14  0.029
#> 15-19     2.88  0.029
#> 22-148    2.74  0.029
#> 21-149    2.52  0.029
#> 20-119    2.29  0.029

# plot results - bubble
plot(res2, pdbs=pdbs)
```

![](man/figures/README-syntax_demo-1.png)

``` r

# boxplot
boxplot(res2, dm=dm, grps=grps)
#> Warning in boxplot.eddm(res2, dm = dm, grps = grps): plotting rows 1 to 20 ( of
#> 30 rows )
```

![](man/figures/README-syntax_demo-2.png)

``` r
pymol(res2, pdbs, grps)
```

![](man/figures/pymol.png)

Citing Bio3D-eDDM
-----------------

-   The Bio3D packages for structural bioinformatics <br> Grant, Skjærven, Yao, (2020) *Protein Science* <br> ( [Abstract](https://onlinelibrary.wiley.com/doi/abs/10.1002/pro.3923) | [PubMed]() | [PDF]() )

Citing Bio3D
------------

-   Integrating protein structural dynamics and evolutionary analysis with Bio3D. <br> Skjærven, Yao, Scarabelli, Grant, (2014) *BMC Bioinformatics* **15**, 399 <br> ( [Abstract](http://www.biomedcentral.com/1471-2105/15/399/abstract) | [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/25491031) | [PDF](http://www.biomedcentral.com/content/pdf/s12859-014-0399-6.pdf) )

-   Bio3D: An R package for the comparative analysis of protein structures. <br> Grant, Rodrigues, ElSawy, McCammon, Caves, (2006) *Bioinformatics* **22**, 2695-2696 <br> ( [Abstract](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/22/21/2695) | [PubMed](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=retrieve&db=pubmed&list_uids=16940322&dopt=Abstract) | [PDF](http://bioinformatics.oxfordjournals.org/content/22/21/2695.full.pdf) )
